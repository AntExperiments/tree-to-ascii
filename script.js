let parse = (str) => {
    return Function(`'use strict'; return (${str})`)()
}

// Input
let input = "(((2*(3+2))+5)/2)";
const reg = /\(([0-9]+)([\+\-\*\/]+)([0-9]+)\)/;

const regL = /\((.+)([\+\/\-\*])([^\+\/\-\*]+)\)/;
const regR = /\(([^\+\/\-\*]+)([\+\/\-\*])(.+)\)/;

let output = [];

// Calculate
var Calculation = function (operator1, operator2, operation) {
    this.operator1 = operator1;
    this.operator2 = operator2;
    this.operation = operation;
};
Calculation.prototype = Object.create(Calculation.prototype);


let checkOperator = (operator) => {
    return (/[()]/.test(operator[1]) ? 1 : 2)
}
let getRegex = (operator) => {
    return (/[()]/.test(operator[1]) ? regL : regR)
}

let get = (tempPath) => {
    tempPath = (tempPath + '').split(';');
    temp = output;

    tempPath.forEach(element => { temp = temp[element] });
    return temp;
}
let set = (tempPath, value) => {
    tempPath = (tempPath + '').split(';');
    temp = output;

    tempPath.forEach(element => { temp = temp[element] });

    let t = JSON.stringify(output);

    // console.log(JSON.stringify(output));

    t= t.replace(`"${temp}"`, JSON.stringify(value));

    output = JSON.parse(t);

    // console.log(t);

    // output.splice(output.indexOf(temp), 1, value);
}

let r = input.match(regL);
output = [r[2], r[1], r[3]];

// let r2 = output[checkOperator(output)].match(getRegex(output[checkOperator(output)]));
// output[checkOperator(output)] = [r2[2], r2[1], r2[3]];

// console.log(output);

// let r3 = output[checkOperator(output)][checkOperator(output[checkOperator(output)])].match(getRegex(output[checkOperator(output)][checkOperator(output[checkOperator(output)])]));
// output[checkOperator(output)][checkOperator(output[checkOperator(output)])] = [r3[2], r3[1], r3[3]];

// console.log(output);

// let r4 = output[checkOperator(output)][checkOperator(output[checkOperator(output)])][checkOperator(output[checkOperator(output)][checkOperator(output[checkOperator(output)])])].match(getRegex(output[checkOperator(output)][checkOperator(output[checkOperator(output)])][checkOperator(output[checkOperator(output)][checkOperator(output[checkOperator(output)])])]));
// output[checkOperator(output)][checkOperator(output[checkOperator(output)])][checkOperator(output[checkOperator(output)][checkOperator(output[checkOperator(output)])])] = [r4[2], r4[1], r4[3]];

console.log(output);

let path = checkOperator(output);
for (let i = 0; i < 2; i++) {
    let r = get(path).match( getRegex( get(path) ) );
    set( path, [r[2], r[1], r[3]]);
    path = path + ";" + checkOperator(output);
    console.log(output);
}

// Output
console.log(output);
// console.log(JSON.stringify(output));
